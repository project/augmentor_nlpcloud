<?php

namespace Drupal\augmentor_nlpcloud\Plugin\Augmentor;

use Drupal\augmentor_nlpcloud\NPLCloudBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * NLP Cloud Text generation augmentor plugin implementation.
 *
 * @Augmentor(
 *   id = "augmentor_nlpcloud_text_generation",
 *   label = @Translation("NLP CLoud Text generation"),
 *   description = @Translation("Start a sentence and let the AI generate the
 *   rest for you, in many languages. You can achieve almost any text processing
 *   and text generation use case thanks to finetuned-llama-2-70b,
 *   dolphin & chatdolphin. You can also use your own model."),
 * )
 */
class NLPCloudTextGeneration extends NPLCloudBase {

  /**
   * Default GPU/CPU status: TRUE (use GPU) / FALSE (use CPU).
   */
  const NLP_CLOUD_GPU = TRUE;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'model' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['model'] = [
      '#type' => 'select',
      '#title' => $this->t('Model'),
      '#options' => $this->getSupportedModels(),
      '#default_value' => $this->configuration['model'] ?? 'finetuned-llama-2-70b',
      '#description' => $this->t('Specifies the model which you want to use for Blog generation.'),
    ];
    // @todo Make the max length dynamic, which is respective to the selected model.
    $form['max_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Max Length'),
      '#default_value' => $this->configuration['max_length'] ?? '1000',
      '#description' => $this->t('The generated output will have a maximum of 2048 tokens for Dolphin and 4096 tokens for Fine-tuned LLaMA 2 70B.'),
      '#states' => [
        'visible' => [
          ['select#edit-settings-model' => ['value' => 'finetuned-llama-2-70b']],
          'or',
          ['select#edit-settings-model' => ['value' => 'dolphin']],
        ],
      ],
    ];

    $form['context'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Context'),
      '#default_value' => $this->configuration['context'] ?? 'Write an informative and engaging article about {input}',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['model'] = $form_state->getValue('model');
    $this->configuration['max_length'] = $form_state->getValue('max_length');
    $this->configuration['context'] = $form_state->getValue('context');
  }

  /**
   * Generates text from the provided input text.
   *
   * @param string $text
   *   The block of text that starts the generated text.
   *   1024 tokens maximum.
   *
   * @return array
   *   The generated text.
   */
  public function execute(string $text): array {
    $context = str_replace('{input}', preg_replace("/[^A-Za-z0-9 ]/", '', $text), $this->configuration['context']);
    try {
      $language = trim($this->configuration['language']);
      $model = trim($this->configuration['model']);
      $client = $this->getClient($model, self::NLP_CLOUD_GPU, $language);
      // articleGeneration() is deprecated,
      // so we should use generation() instead to generate a blog content.
      $result = $client->generation(
        $context,
        (int) $this->configuration['max_length'],
        TRUE,
        NULL,
        TRUE,
        1,
        1,
        50,
        1,
        0.8,
        1,
        NULL,
        FALSE
      );
      return ['default' => $result->generated_text];
    }
    catch (\Throwable $error) {
      $this->logger->error('NLP Cloud text generation error: %message.', [
        '%message' => $error->getMessage(),
      ]);
      return [
        '_errors' => $this->t('Error during the NLP Cloud text generation, please check the logs for more information.')->render(),
      ];
    }
  }

  /**
   * Returns the list of supported models by Text generation.
   *
   * @return array
   *   With the list of supported models.
   */
  private function getSupportedModels(): array {
    return [
      'finetuned-llama-2-70b' => $this->t('Fine-tuned LLaMA 2 70B'),
      'chatdolphin' => $this->t('ChatDolphin'),
      'dolphin' => $this->t('Dolphin'),
    ];
  }

}
