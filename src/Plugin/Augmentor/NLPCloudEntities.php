<?php

namespace Drupal\augmentor_nlpcloud\Plugin\Augmentor;

use Drupal\augmentor_nlpcloud\NPLCloudBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * NLP Cloud Entities augmentor plugin implementation.
 *
 * @Augmentor(
 *   id = "augmentor_nlpcloud_entities",
 *   label = @Translation("NLP Cloud Entities extraction (NER)"),
 *   description = @Translation("Entity extraction (also known as Named Entity Recognition (NER))."),
 * )
 */
class NLPCloudEntities extends NPLCloudBase {

  /**
   * Default GPU/CPU status: TRUE (use GPU) / FALSE (use CPU).
   */
  const NLP_CLOUD_GPU = FALSE;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'model' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['model'] = [
      '#type' => 'select',
      '#title' => $this->t('Model'),
      '#options' => $this->getSupportedModels(),
      '#default_value' => $this->configuration['model'] ?? 'fast-gpt-j',
      '#description' => $this->t("Specifies the model which you want to use for entities extraction."),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['model'] = $form_state->getValue('model');
  }

  /**
   * Extracts entities from the provided input text.
   *
   * @param string $text
   *   The text you want to extract entities from.
   *
   * @return array
   *   The main keywords and keyphrases in your text.
   */
  public function execute(string $text): array {
    try {
      $language = trim($this->configuration['language']);
      $model = trim($this->configuration['model']);
      $client = $this->getClient($model, self::NLP_CLOUD_GPU, $language);
      // @todo Limit to the first 1024 chars as an approximation of 256 tokens.
      // Limited to 256 tokens in synchronous mode.
      $text = substr($text, 0, 1024);
      $result = $client->entities($text);
      return ['default' => json_encode($result->entities)];
    }
    catch (\Throwable $error) {
      $this->logger->error('NLP Cloud entities extraction error: %message.', [
        '%message' => $error->getMessage(),
      ]);
      return [
        '_errors' => $this->t('Error during the NLP Cloud entities extraction, please check the logs for more information.')->render(),
      ];
    }
  }

  /**
   * Returns the list of supported models by Keywords and Keyphrases Extraction.
   *
   * @return array
   *   With the list of supported models.
   */
  private function getSupportedModels(): array {
    return [
      'en_core_web_lg' => $this->t("spaCy's English Large"),
      'fast-gpt-j' => $this->t('Fast GPT-J'),
      'finetuned-gpt-neox-20b' => $this->t('Finetuned GPT-NeoX 20B'),
    ];
  }

}
